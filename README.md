# binary search tree #
A Python implementation of a binary search tree and corresponding node object.

## Full Documentation ##
Sphinx-generated documentation is available in docs/_build/index.html.

## Installation ##
Clone this repository or download and extract it anywhere on a local machine with Python 3.x installed.

## Usage ##
Run: py -m binary_search_tree from the project's root directory. Follow the on-screen prompts.

## Concepts demonstrated ##
+ Understanding of the merge-sort algorithm and the sorting problem's optimal substructure.
+ General grasp of Python language and program control structures.
+ Understanding of unit testing and test-driven development methodology.
+ Familiarity with principles of recursion.
+ Awareness of clean code concepts.
+ Grasp of Singleton design pattern.
+ Familiarity with regular expressions.
+ Understanding of closures as method of implementing encapsulation.
+ Use of Git version control system.
+ Use of Sphinx documentation generator.


