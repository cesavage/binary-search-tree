from binary_search_tree.modules.binary_search_tree import BinarySearchTree
from binary_search_tree.modules.binary_search_tree import TreeNode
import binary_search_tree.modules.appStrings as appStrings
from binary_search_tree.modules.menu.menuOption import MenuOption
from binary_search_tree.modules.menu.menu import Menu
from binary_search_tree.modules.data_validator import DataValidator
from binary_search_tree.modules.data_validator import ValidationException

def promptUserToContinue():
    input("(Press [enter] to continue.)")


def _menuOption_traverse_inOrder():
    print("In-order tree traversal")
    print("-----------------------")
    
    traversalList = bsTree.getTraversalList_inOrder()
    
    print("TRAVERSAL COMPLETE: In-order traversal list: " + str( traversalList ) )
    promptUserToContinue()
    return


def _menuOption_traverse_preOrder():
    print("Pre-order tree traversal")
    print("------------------------")

    traversalList = bsTree.getTraversalList_preorder()

    print("TRAVERSAL COMPLETE: Pre-order traversal list: " + str( traversalList ) )
    promptUserToContinue()
    return


def _menuOption_traverse_postOrder():
    print("Post-order tree traversal")
    print("-------------------------")

    traversalList = bsTree.getTraversalList_postOrder()

    print("Post-order traversal list: " + str( traversalList ) )
    promptUserToContinue()
    return


def _menuOption_findNode():
    print("Find a Node")
    print("-----------")
    targetNode = input("Enter the value of the node to search for: ")
    searchResult = bsTree.findNode(targetNode)

    if type(searchResult) is TreeNode:
        print("\nNODE FOUND: " + "The value " + targetNode + " was found in the tree.")
        print("")
    else:
        print("\nNODE NOT FOUND: " + "The value " + targetNode + " was not found in the tree.")
    
    promptUserToContinue()
    return


def _menuOption_createNewTree():
    #nonlocal bsTree
    global bsTree
    bsTree = None
    print("Existing tree deleted.")
    promptUserToContinue()


def _menuOption_deleteNode():
    print("Delete a node")
    print("-------------")
    deletionTarget = input("Enter the value of the node to delete: ")

    deleteResult = bsTree.deleteNode(deletionTarget)
    if deleteResult is True:
        print("\nDELETION COMPLETE: " + str( deletionTarget) + " has been removed from the tree.")
        promptUserToContinue()


def _exit():
    exit()


def createMenu():
    treeMenu = Menu()

    findNode_description = "Find a node in the tree."
    findNode_callback = _menuOption_findNode
    findNode = MenuOption(findNode_description, findNode_callback)

    traverse_inOrder_description = "Perform in-order traversal of the tree."
    traverse_inOrder_callback = _menuOption_traverse_inOrder
    traverse_inOrder = MenuOption(traverse_inOrder_description, traverse_inOrder_callback)

    traverse_preOrder_decription = "Perform in pre-order traversal of the tree."
    traverse_preOrder_callback = _menuOption_traverse_preOrder
    traverse_preOrder = MenuOption(traverse_preOrder_decription, traverse_preOrder_callback)

    traverse_postOrder_description = "Perform a post-order traversal of the tree."
    traverse_postOrder_callback = _menuOption_traverse_postOrder
    traverse_postOrder = MenuOption(traverse_postOrder_description, traverse_postOrder_callback)

    createNewTree_description = "Create a new tree."
    createNewTree_callback = _menuOption_createNewTree
    createNewTree = MenuOption(createNewTree_description, createNewTree_callback)

    exitOption_description = "Exit the program."
    exitOption_callback = _exit
    exitOption = MenuOption(exitOption_description, exitOption_callback)

    deleteNode_description = "Delete a node from the tree."
    deleteNode_callback = _menuOption_deleteNode
    deleteNode = MenuOption(deleteNode_description, deleteNode_callback)
    
    treeMenu.menuOptions.append(findNode)
    treeMenu.menuOptions.append(deleteNode)
    treeMenu.menuOptions.append(traverse_inOrder)
    treeMenu.menuOptions.append(traverse_preOrder)
    treeMenu.menuOptions.append(traverse_postOrder)
    treeMenu.menuOptions.append(createNewTree)
    treeMenu.menuOptions.append(exitOption)

    return treeMenu


def validateUserInput(userInput):
    
    try:
        userInput = list( map(DataValidator.checkUserInputIsInt,userInput) )

    except ValidationException as dataValidationException:
        print(dataValidationException.message)
        return dataValidationException    
    
    return userInput


def populateTreeFromIntList(tree, integerList):
    for integer in integerList:
        tree.insertNode(TreeNode(integer))
    return tree



if __name__ == "__main__":

    treeMenu = createMenu()

    while True:
        print("\nBinary Search Tree")
        print("------------------")
        userInput = input("Enter a comma-separated list of integers to create a binary search tree (or 'exit' to quit): ")

        if userInput == appStrings.EXIT_KEYWORD:
            exit()

        userInput = list( userInput.split(','))
        
        if type(validateUserInput(userInput)) is ValidationException:
            continue
        
        bsTree = BinarySearchTree()
        bsTree = populateTreeFromIntList(bsTree, userInput)

        while bsTree is not None:
            treeMenu.printMenu()
            userSelection = input("Make a selection from the Tree Menu: ")
            userSelection = int(userSelection)
            treeMenu.executeMenuChoice(userSelection)