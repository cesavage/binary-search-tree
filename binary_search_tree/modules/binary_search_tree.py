from binary_search_tree.modules.tree_node import TreeNode
from binary_search_tree.modules.data_validator import ValidationException
class BinarySearchTree:

    def __init__(self):
        self.root = None

    def insertNode(self, newNode, currentNode = "root"):
        """
        Insert a node into it's proper location in the tree.

        The default value of the currentNode parameter is "root," 
        which is intended to be used by api consumers.
        The ability to specify the curernt node only exists to enable recursion.

        :param newNode: The new node to be inserted into the tree.
        :type: TreeNode

        :param currentNode: The node at which the new node should be inserted.
        :type: TreeNode:

        :returns: True if the node was inserted. False if the node was not inserted.
        :rtype: bool

        """

        def _treeHasRoot():
            if self.root is not None:
                return True
            else:
                return False


        def _startInsertionFromRoot():
            nodeInserted = False
            if _treeHasRoot() == True:
                nodeInserted = self.insertNode(newNode, self.root)

            else:
                self.root = newNode
                nodeInserted = True
            return nodeInserted
        

        def _leftChildIsParentCandidate():
            if currentNode.getLeftChild() and newNode.getValue() <= currentNode.getValue():
                return True
            else:
                return False
            

        def _rightChildIsParentCandidate():
            if currentNode.getRightChild() and newNode.getValue() > currentNode.getValue():
                return True
            else:
                return False


        def _getNextParentCandidateNode():
            if _leftChildIsParentCandidate():
                return currentNode.getLeftChild()

            elif _rightChildIsParentCandidate():
                return currentNode.getRightChild()

            else:
                return None

        nodeInserted = False

        if currentNode == "root":
            nodeInserted = _startInsertionFromRoot()

        elif currentNode.canBeParentOf(newNode) is True:
            currentNode.addChild(newNode)
            if currentNode.getLeftChild() is newNode or currentNode.getRightChild() is newNode:
                nodeInserted = True

        elif currentNode.canBeParentOf(newNode) is False:
                nextParentCandidateNode = _getNextParentCandidateNode()
                nodeInserted = self.insertNode(newNode, nextParentCandidateNode)
        
        return nodeInserted  

    def getTraversalList_inOrder(self):
        """
        Get a list of all integers from the tree in the order of an 
        in-order tree traversal.

        :returns: A list of all tree values (int) in the order of an in_order tree traversal.

        """
                    
        def _getTraversalList_inOrder(startingNode):
            nonlocal traversalList

            if startingNode.hasLeftChild():
                traversalList = _getTraversalList_inOrder(startingNode.getLeftChild())

            traversalList.append(startingNode.getValue())

            if startingNode.hasRightChild():
                traversalList = _getTraversalList_inOrder(startingNode.getRightChild())
                    
            return traversalList

        traversalList = []
        startingNode = self.root

        return _getTraversalList_inOrder(startingNode)


    def getTraversalList_preorder(self):
        """
        Get a list of all integers from the tree in the order of an 
        pre-order tree traversal.

        :returns: A list of all tree values (int) in the order of an pre_order tree traversal.

        """

        def _getTraversalList_preorder(currentNode):
            nonlocal traversalList

            traversalList.append(currentNode.getValue())

            if currentNode.hasLeftChild():
                traversalList = _getTraversalList_preorder(currentNode.getLeftChild())

            if currentNode.hasRightChild():
                traversalList = _getTraversalList_preorder(currentNode.getRightChild())

            return traversalList


        traversalList = []
        currentNode = self.root

        return _getTraversalList_preorder(currentNode)


    def getTraversalList_postOrder(self):
        """
        Get a list of all integers from the tree in the order of a 
        post-order tree traversal.

        :returns: A list of all tree values (int) in the order of a post-order tree traversal.

        """

        def _getTraversalLlist_postOrder(currentNode):
            if currentNode.hasLeftChild():
                _getTraversalLlist_postOrder(currentNode.getLeftChild())

            if currentNode.hasRightChild():
                _getTraversalLlist_postOrder(currentNode.getRightChild())

            traversalList.append(currentNode.getValue())

            return traversalList

        traversalList = []
        currentNode = self.root

        return _getTraversalLlist_postOrder(currentNode)


    def findNode(self, searchValue, currentNode = "root"):
        """
        Get a specific node from the tree.

        The default value of currentNode is "root", to begin searching
        at the root node. This is the expected use of the api.
        The ability to specify a currentNode exists to allow for 
        recursion.

        :param searchValue: The value of the node to be returned.
        :type: int

        :param currentNode: The node at which to begin searching.
        :type: TreeNode

        :returns: The tree node with a value that matches searchValue or None of the node is not found.
        :rtype: TreeNode or None
        """
        def _getNextCandidateNode():
            if searchValue < currentNode.getValue() and currentNode.hasLeftChild():
                return currentNode.getLeftChild()
            elif searchValue > currentNode.getValue() and currentNode.hasRightChild():
                return currentNode.getRightChild()
            else:
                return None
        
        
        foundNode = None

        if currentNode == "root":
            foundNode = self.findNode(searchValue, self.root)
        elif currentNode.getValue() == searchValue:
            foundNode = currentNode
        elif _getNextCandidateNode() is not None:
            foundNode = self.findNode(searchValue, _getNextCandidateNode())            

        return foundNode

    def getSmallestChildFromNode(self, currentNode):
        if currentNode.hasLeftChild() is False:
            return currentNode

        else:
            return self.getSmallestChildFromNode(currentNode.getLeftChild())


    def _deleteRootNode(self):
        if self.root.getLeftChild() is None and self.root.getRightChild() is None:
            print("\nERROR: This is the only node  in the tree. Insert a new node before deleting this one.")
            return
            
        elif self.root.hasRightChild() is True:
            smallestNode = self.getSmallestChildFromNode(self.root.getRightChild())
            smallestValue = smallestNode.getValue()
            self.deleteNode(smallestValue)

            try:
                self.root.setValue(smallestValue)
            except ValidationException as dataValidationException:
                print(dataValidationException.message)
            

        elif self.root.hasLeftChild() and self.root.getLeftChild().hasRightChild() is True:
            smallestNode = self.getSmallestChildFromNode(self.root.getLeftChild().getRightChild())
            smallestValue = smallestNode.getValue()
            self.deleteNode(smallestValue)
            self.root.setValue(smallestValue)

        elif self.root.hasLeftChild() and self.root.getLeftChild().hasRightChild() is False:
            smallestNode = self.root.getLeftChild()
            smallestValue = smallestNode.getValue()
            self.deleteNode(smallestValue)
            self.root.setValue(smallestValue)

        

    def deleteNode(self, deletionValue, currentNode = "root", parentNode = None):
        """
        Delete a node from the tree.

        The default value of currentNode is "root" and parentNode is None, 
        to begin deletion at the root node. This is the expected use of the api.
        The ability to specify a currentNode exists to allow for 
        recursion.

        :param deletionValue: The value of the node to be deleted.
        :type: int

        :param currentNode: The node at which to begin the recursive deletion routine.
        :type: TreeNode

        :param parentNode: The parent of the currentNode.
        :type: TreeNode
        
        :returns: True if the node was deleted or False if the node could not be found.
        :rtype: bool
        """
        def _executeNodeRemoval():
            if currentNode is self.root:
               self._deleteRootNode()

            else: parentNode.removeChild(currentNode)
        
        nodeDeleted = False

        if currentNode == "root":
            nodeDeleted = self.deleteNode(deletionValue, self.root)    
        
        elif deletionValue < currentNode.getValue():
            nodeDeleted = self.deleteNode(deletionValue, currentNode.getLeftChild(), currentNode)
            pass
        elif deletionValue > currentNode.getValue() and currentNode.hasRightChild():
            nodeDeleted = self.deleteNode(deletionValue, currentNode.getRightChild(), currentNode)
            pass
        elif deletionValue == currentNode.getValue():
            _executeNodeRemoval()
            nodeDeleted = True

        return nodeDeleted