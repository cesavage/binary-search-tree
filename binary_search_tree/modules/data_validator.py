import binary_search_tree.modules.appStrings as appStrings
import re as regex


class ValidationException(Exception):
    message="\nERROR: Please enter only integers or '" + appStrings.EXIT_KEYWORD + "' to exit."



class DataValidator:
    """
    Class used for validation of user input.
    """
    @staticmethod
    def checkUserInputIsInt(inputValue):
        """
        Checks that the supplied parameteter is an integer.

        :param inputValue: The user-supplied value to be validated.
        :type: string
        
        :raises: ValidationException

        :returns: The value from the input parameter cast as an integer.
        :rtype: int
        """
        try:
            intValue = int(inputValue)
        except ValueError:
            raise ValidationException()

        return intValue



    @staticmethod
    def cleanUserInput(userInput):
        """
        Removes extranneous commas from the start of end of the string parameter.

        :param userInput: The user's input
        :type: string

        :returns: The input parameter with cleanup regex applied.
        :rtype: string
        """
        userInput = regex.sub( '^,|,$', '', userInput )
        
        return userInput