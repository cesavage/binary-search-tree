from .menuOption import MenuOption

class Menu():
    menuOptions = []

    def printMenu(self):
        """
        Assigns a menu number to each entry and prints the menu.
        """
        menuCounter = 1
        print("\nTree Menu")
        print("---------")
        for menuOption in self.menuOptions:
            print( str(menuCounter) + ": " +  menuOption.description)
            menuCounter += 1
        print("")

    def executeMenuChoice(self, menuChoice):
        """
        Returns the callback to be executed for the selected menu choice.

        :param menuChoice: The number of the menu choice entered by the user.
        :type: str

        :returns: A reference to the callback function to be executed.
        :rtype: function object reference
        """
        menuOptionListItem = menuChoice -1
        print("")
        return self.menuOptions[menuOptionListItem].callback() 