class MenuOption:
    def __init__(self, description, callback):
        """
        Creates a menu option to be displayed in a menu.

        :param description: The name of the enu entry.
        :type: string

        :param callback: The callback method to be executed when this menu item is selected.
        :type: function reference

        :returns: A menu option object.
        :rtype: MenuOption
        """
        self.description = description
        self.callback = callback