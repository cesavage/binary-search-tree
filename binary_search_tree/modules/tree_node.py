from binary_search_tree.modules.data_validator import DataValidator
from binary_search_tree.modules.data_validator import ValidationException

class TreeNode:
    #Public methods are defined here to allow documentation
    #auto-generator to discover them.

    def getValue(self):
        """
        Returns the private value attribute of the node.

        :return: The value of the node.
        :rtype: int
        """
        return None

    def setValue(self, newValue):
        """
        Sets the value of the node.

        :param value: The value to be set for the node.
        :type: int
        """
        return None

    def getLeftChild(self):
        """
        Get the left child of the node.

        :return: The left child of the node.
        :rtype: TreeNode
        """
        return None

    def getRightChild(self):
        """
        Get the right child of the node.

        :return: The right child of the node.
        :rtype: TreeNode
        """
        return None
    
    def hasLeftChild(self):
        """
        Deteremine if the node has a left child.

        :returns: True if the node has a left child.
        :rtype: bool
        """
        return None

    def hasRightChild(self):
        """
        Deteremine if if the node has a right child.

        :returns: True if the node has a right child.
        :rtype: bool
        """
        return None

    def addChild(self, newNode):
        """
        Evaluates the new node's value and places it as a left or right child.

        :param newNode: The node to be added as a child.
        :type: TreeNode
        """
        return None

    def removeChild(self, childNode):
        """
        Removes a child from a node.

        :param childNode: The child node to be removed.
        :type: TreeNode
        """
        return None
    
    def canBeParentOf(self, newNode):
        """
        Determine if the node can accept a child where the new node would be placed.

        :param newNode: The potential child node.
        :type: TreeNode
        :returns: True if the node can accept newNode as a child.
        :rtype: bool
        """
        return None


    def __init__(self, value):
    
        def definePrivateAttributesAndMethods(self, value):
            """
            Defines private object attributes and the definitions of private
            attribute accessors exposed publicly.
            """
            nodeValue = value
            leftChild = None
            rightChild = None

            def _getValue():
                nonlocal nodeValue
                return nodeValue
            

            def _setValue(newValue):
                def _checkValueIsValidForNode():
                    try: DataValidator.checkUserInputIsInt(newValue)
                    except ValueError as valueError:
                        raise ValidationException()

                    if _hasLeftChild() and _getLeftChild().getValue() > newValue:
                        raise Exception("This insertion would violate tree integrity. The new value " + newValue+ " is less than the value of the left child's value of " + _getLeftChild().getValue() + ".")

                    if _hasRightChild() and _getRightChild().getValue() < newValue:
                        raise Exception("This insertion would violate tree integrity. The new value " + newValue + " is greater than the right child's value of " + _getRightChild().getValue() + ".") 


                _checkValueIsValidForNode()
                nonlocal nodeValue
                nodeValue = newValue


            def _hasLeftChild():
                nonlocal leftChild
                if leftChild:
                    return True
                else:
                    return False


            def _hasRightChild():
                nonlocal rightChild
                if rightChild:
                    return True
                else:
                    return False

        
            def _canBeParentOf(newNodeR):
                canBeParentOf = False

                if newNodeR.getValue() <= _getValue() and _getLeftChild() is None:
                    canBeParentOf = True
                elif newNodeR.getValue() > _getValue() and _getRightChild() is None:
                    canBeParentOf = True

                return canBeParentOf


            def _getLeftChild():
                nonlocal leftChild
                return leftChild


            def _getRightChild():
                nonlocal rightChild
                return rightChild


            def _setLeftChild(newNode):
                def _checkIfNewNodeIsValidLeftChild():
                    if newNode is not None and newNode.getValue() > _getValue():
                        raise Exception("Inserting node with value " + newNode.getValue() + " as left child of node with value " + _getValue() + " compromises the binary search tree's hierarchy.")

                _checkIfNewNodeIsValidLeftChild()
                nonlocal leftChild
                leftChild = newNode


            def _setRightChild(newNode):
                def _checkIfNewNodeIsValidRightChild():
                    if newNode is not None and newNode.getValue() < _getValue():
                        raise Exception("Inserting node with value " + newNode.getValue() + " as right child of node with value " + _getValue() + " compromises the binary search tree's hierarchy.")

                _checkIfNewNodeIsValidRightChild()
                nonlocal rightChild
                rightChild = newNode


            def _addChild(newNode):
                if newNode.getValue() <= _getValue():
                    _setLeftChild(newNode)
                elif newNode.getValue() > _getValue():
                    _setRightChild(newNode)

            
            def _getSmallestNodeFrom(node):
                if node.hasLeftChild():
                    return _getSmallestNodeFrom(node.getLeftChild())
                else:
                    return node


            def _getParentOfSmallestRightChild(node):
                if node.hasLeftChild() is False:
                    return None

                elif node.hasLeftChild is True and node.getLeftChild().hasLeftChild() is False:
                    return node

                elif node.hasLeftChild is True and node.getLeftChild().hasLeftChild() is True:
                    return _getParentOfSmallestRightChild(node.getLeftChild())            


            def _removeChild(deletionTarget):

                def _findReplacmentNodeParent(deletionTarget):
                    if deletionTarget.getRightChild().hasLeftChild() is False:
                        return deletionTarget

                    elif deletionTarget.getRightChild().hasLeftChild() is True and deletionTarget.getRightChild().getLeftChild().hasLeftChild() is False:
                        return deletionTarget.getRightChild()

                    elif deletionTarget.getRightChild().hasLeftChild() and deletionTarget.getRightChild().getLeftChild().hasLeftChild() is True:
                        return _findReplacmentNodeParent(deletionTarget.getRightChild.getLeftChild)


                def _getReplacementNodeFromParent(replacementNodeParent, deletionTarget):
                    replacementNode = None
                    
                    if replacementNodeParent is deletionTarget:
                        replacementNode = deletionTarget.getRightChild()
                    else:
                        replacementNode = replacementNodeParent.getLeftChild()
                    
                    return replacementNode


                def _replaceDeletionTargetWithNextLargestNode(deletionTarget):
                    replacementNode_parent = _findReplacmentNodeParent(deletionTarget)
                    replacementNode = _getReplacementNodeFromParent(replacementNode_parent, deletionTarget)
                    replacementNode_value = replacementNode.getValue()

                    replacementNode_parent.removeChild(replacementNode)
                    deletionTarget.setValue(replacementNode_value)


                def _removeLeftChild():
                    if deletionTarget.hasLeftChild() is False and deletionTarget.hasRightChild() is False:
                        _setLeftChild(None)

                    elif deletionTarget.hasLeftChild() is True and deletionTarget.hasRightChild() is False:
                        _setLeftChild(deletionTarget.getLeftChild())

                    elif deletionTarget.hasLeftChild() is False and deletionTarget.hasRightChild() is True:
                        _setLeftChild(deletionTarget.getRightChild())

                    elif deletionTarget.hasLeftChild() is True and deletionTarget.hasRightChild() is True:
                        _replaceDeletionTargetWithNextLargestNode(deletionTarget)

                        
                def _removeRightChild():
                    if deletionTarget.hasLeftChild() is False and deletionTarget.hasRightChild() is False:
                        _setRightChild(None)

                    elif deletionTarget.hasLeftChild() is True and deletionTarget.hasRightChild() is False:
                        _setRightChild(deletionTarget.getLeftChild())

                    elif deletionTarget.hasLeftChild() is False and deletionTarget.hasRightChild() is True:
                        _setRightChild(deletionTarget.getRightChild())

                    elif deletionTarget.hasLeftChild() is True and deletionTarget.hasRightChild() is True:
                        _replaceDeletionTargetWithNextLargestNode(deletionTarget)

                if deletionTarget.getValue() <= _getValue():
                    _removeLeftChild()
                    
                elif deletionTarget.getValue() > _getValue():
                    _removeRightChild()
                    

            def _exposePublicMethods():
                self.addChild = _addChild
                self.removeChild = _removeChild
                self.getValue = _getValue
                self.setValue = _setValue
                self.getRightChild = _getRightChild
                self.getLeftChild = _getLeftChild
                self.canBeParentOf = _canBeParentOf
                self.hasRightChild = _hasRightChild
                self.hasLeftChild = _hasLeftChild


            _exposePublicMethods()


        definePrivateAttributesAndMethods(self, value)
        self.value = self.getValue()