import unittest
from binary_search_tree.modules.tree_node import TreeNode as Node
from binary_search_tree.modules.binary_search_tree import BinarySearchTree as BSTree




class Test_binary_search_tree(unittest.TestCase):
    def createTestTree(self):

        """    
              50
         /          \
        40          60
       /  \        /  \
      30  45      55  70
      /    \       \   /
     25    48      58 65
    """
        binarySearchTree = BSTree()

        binarySearchTree.insertNode( Node(50) )
        binarySearchTree.insertNode( Node(40) )
        binarySearchTree.insertNode( Node(30) )
        binarySearchTree.insertNode( Node(45) )
        binarySearchTree.insertNode( Node(25) )
        binarySearchTree.insertNode( Node(48) )
        binarySearchTree.insertNode( Node(60) )
        binarySearchTree.insertNode( Node(55) )
        binarySearchTree.insertNode( Node(70) )
        binarySearchTree.insertNode( Node(58) )
        binarySearchTree.insertNode( Node(65) )
        
        return binarySearchTree
    
    def test_insertNode(self):
        bsTree = BSTree()
        bsTree.insertNode( Node(50) )
        bsTree.insertNode( Node(25) )
        self.assertEqual(bsTree.insertNode( Node(75)), True) 

        rootNodeLeftChild = bsTree.root.getLeftChild()
        rootNodeRightChild = bsTree.root.getRightChild()
        
        self.assertEqual(rootNodeLeftChild.getValue(), 25)
        self.assertEqual(rootNodeRightChild.getValue(), 75)



    def test_traverse_inOrder(self):
        binarySearchTree = self.createTestTree()
        traversalList = []

        traversalList = binarySearchTree.getTraversalList_inOrder()
        
        self.assertEqual(traversalList, [25,30,40,45,48,50,55,58,60,65,70])



    def test_traverse_preOrder(self):
        binarySearchTree = self.createTestTree()
        traversalList = []
        
        traversalList = binarySearchTree.getTraversalList_preorder()
        self.assertEqual(traversalList, [50,40,30,25,45,48,60,55,58,70,65])


        
    def test_traverse_postOrder(self):
        binarySearchTree = self.createTestTree()
        traversalList = []
        
        traversalList = binarySearchTree.getTraversalList_postOrder()
        self.assertEqual(traversalList, [25,30,48,45,40,58,55,65,70,60,50])

    
    def test_findNode(self):
        binarySearchTree = self.createTestTree()
        
        foundNode = binarySearchTree.findNode(50)
        self.assertEqual(foundNode.getValue(), 50)

        foundNode = binarySearchTree.findNode(55)
        self.assertEqual(foundNode.getValue(), 55)

        foundNode = binarySearchTree.findNode(99)
        self.assertEqual(foundNode, None)


    def test_getSmallestValueFromNode(self):
        binarySearchTree = self.createTestTree()

        smallestNode = binarySearchTree.getSmallestChildFromNode(binarySearchTree.root)

        self.assertEqual(smallestNode.getValue(), 25)

    
    def test_deleteNode(self):
        # #Delete a LEFT NODE LEAF
        binarySearchTree = self.createTestTree()

        binarySearchTree.deleteNode(65)

        traversalList = []
        postDelete_traversal = binarySearchTree.getTraversalList_inOrder()
        self.assertEqual(postDelete_traversal, [25,30,40,45,48,50,55,58,60,70])


    #     # #Delete RIGHT NODE LEAF
    #     binarySearchTree = self.createTestTree()

    #     binarySearchTree.deleteNode(58)

    #     traversalList = []
    #     postDelete_traversal = binarySearchTree.getTraversalList_inOrder()
    #     self.assertEqual(postDelete_traversal, [25,30,40,45,48,50,55,60,65,70])


    #     # #Delete a LEFT NODE with LEFT CHILD
    #     binarySearchTree = self.createTestTree()

    #     binarySearchTree.deleteNode(30)
    #     traversalList = []
    #     postDelete_traversal = binarySearchTree.getTraversalList_inOrder()
    #     self.assertEqual(postDelete_traversal, [25,40,45,48,50,55,58,60,65,70])


    #     # #Delete LEFT NODE with RIGHT CHILD
    #     binarySearchTree = self.createTestTree()

    #     binarySearchTree.deleteNode(55)
    #     traversalList = []
    #     postDelete_traversal = binarySearchTree.getTraversalList_inOrder()
    #     self.assertEqual(postDelete_traversal, [25,30,40,45,48,50,58,60,65,70])


    #     # #Delete RIGHT NODE with LEFT CHILD
    #     binarySearchTree = self.createTestTree()

    #     binarySearchTree.deleteNode(70)

    #     traversalList = []
    #     postDelete_traversal = binarySearchTree.getTraversalList_inOrder()
    #     self.assertEqual(postDelete_traversal, [25,30,40,45,48,50,55,58,60,65])


    #     # #Delete RIGHT NODE with RIGHT CHILD
    #     binarySearchTree = self.createTestTree()
    #     binarySearchTree.deleteNode(45)

    #     traversalList = []
    #     postDelete_traversal = binarySearchTree.getTraversalList_inOrder()
    #     self.assertEqual(postDelete_traversal, [25,30,40,48,50,55,58,60,65,70])


    #     # #Delete LEFT NODE with LEFT AND RIGHT CHILD
    #     binarySearchTree = self.createTestTree()

    #     binarySearchTree.deleteNode(40)
    #     traversalList = []
    #     postDelete_traversal = binarySearchTree.getTraversalList_inOrder()
    #     self.assertEqual(postDelete_traversal, [25,30,45,48,50,55,58,60,65,70])



    #     #Delete RIGHT NODE with LEFT AND RIGHT CHILD
    #     binarySearchTree = self.createTestTree()
    #     binarySearchTree.deleteNode(60)

    #     traversalList = []
    #     postDelete_traversal = binarySearchTree.getTraversalList_inOrder()
    #     self.assertEqual(postDelete_traversal, [25,30,40,45,48,50,55,58,65,70])


    #     #Delete root node
    #     #Reported in issue 1.
    #     bsTree = BSTree()
    #     bsTree.insertNode( Node(1) )
    #     bsTree.insertNode( Node(2) )
    #     bsTree.insertNode( Node(3) )

    #     bsTree.deleteNode(1)
        

    #     traversalList = []
    #     traversalList = bsTree.getTraversalList_inOrder()
    #     self.assertEqual(traversalList, [2,3])

    # def test_deleteRootOfLeftTree(self):
    #     bsTree = BSTree()
    #     bsTree.insertNode( Node(3) )
    #     bsTree.insertNode( Node(2) )
    #     bsTree.insertNode( Node(1) )

    #     bsTree.deleteNode(3)
        

    #     traversalList = []
    #     traversalList = bsTree.getTraversalList_inOrder()
    #     self.assertEqual(traversalList, [1,2])


        


















        

    
