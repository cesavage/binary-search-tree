import unittest
from binary_search_tree.modules.binary_search_tree import BinarySearchTree as BSTree
from binary_search_tree.modules.tree_node import TreeNode
from binary_search_tree.modules.data_validator import ValidationException

class Test_TreeNode(unittest.TestCase):

    def test_setValue(self):
        node = TreeNode(50)

        #Test setting a non-numeric value.
        with self.assertRaises(ValidationException):
            node.setValue('a')
        
        node = TreeNode(50)
        node.addChild(TreeNode(25))
        node.addChild(TreeNode(75))

        #Test setting a value that preserves the tree structure.
        node.setValue(45)
        self.assertEqual(node.getValue(), 45)

        #Test setting a value that violates the tree structure from the left child.
        with self.assertRaises(Exception):
            node.setValue(15)

        #Test setting a value that volates the tree structure from the right child.
        with self.assertRaises(Exception):
            node.setValue(85)

    def test_createNode(self):
        node = TreeNode(50)
        self.assertEqual(node.getValue(), 50)


    def test_getValue(self):
        node = TreeNode(50)
        self.assertEqual(node.getValue(), 50)

    
    def test_getLeftChild(self):
        bsTree = BSTree()
        bsTree.insertNode( TreeNode(50) )
        bsTree.insertNode( TreeNode(25) )

        treeRoot = bsTree.root
        rootLeftChild = treeRoot.getLeftChild()
        
        self.assertEqual(rootLeftChild.getValue(), 25)


    def test_getRightChild(self):
        bsTree = BSTree()
        bsTree.insertNode (TreeNode(50))
        bsTree.insertNode (TreeNode(75))

        treeRoot = bsTree.root
        rootRightChild = treeRoot.getRightChild()
        
        self.assertEqual(rootRightChild.getValue(), 75)


    def test_canBeParentOf(self):
        bsTree = BSTree()
        bsTree.insertNode (TreeNode(50))
        bsTree.insertNode (TreeNode(25))

        treeRoot = bsTree.root
        falseTestChild = TreeNode(15)
        trueTestChild = TreeNode(75)

        self.assertEqual(treeRoot.canBeParentOf(falseTestChild), False)
        self.assertEqual(treeRoot.canBeParentOf(trueTestChild), True)