.. Binary Search Tree documentation master file, created by
   sphinx-quickstart on Sat Jan  4 20:38:48 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Binary Search Tree
==================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   algorithm
   concepts

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
