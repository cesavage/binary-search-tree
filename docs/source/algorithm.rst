The Binary Search Tree
========================

The binary search tree is a specially-organized tree structure containing comparative data. In it's simplest form, a binary search tree is no different than any other binary tree wherein each of nod the tree consists of a value attribute, a reference to the node's left child and a reference to the node's right child.

A binary search tree must meet the criteria of a binary tree, but must also have its nodes organized in such a way that the value of each child node to the left of a given parent is less than or equal to its parent's value, and the value each child node to the right of a given parent is greater than its parent's value. This organization of the binary search tree's nodes make search operations more efficient.

Binary Search Trees are especially useful in priority queue applications, where insertion and lookup operations are both common.

Tree structure
^^^^^^^^^^^^^^
A binary tree's structure is comprised of two elements: a root node and the node object itself.

A node is an object consisting of at least the following three attributes:
- A value
- A pointer to a left child node
- A pointer to a right chid node

By establishing a connection between each parent and its children, the hierarchy of the tree is established in much the same way the order of items is established in a linked list.

The root node is special in that no other node points to it as its left or right child. The root node is at the top of the hierarchy of the binary search tree, and every other node is accessible from it.

Tree Operations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Node Search
-----------
Node search leverages the special, hierarchical nature of the binary search tree. Nodes are found by beginning at the root node, and traversing the binary search tree dependent upon if the target search value is less than or greater than each node the algorithm visits. If the search target is greater than the node visted, the search continues with that node's right child and vice-versa.


Node insertion
--------------
In a binary search trees, nodes are always inserted as leafs and are placed according to the rules of a binary search tree. This is beneficail because it avoids needing to restructure the tree each time a new value is added. Inserting a node consists of setting a pointer to a child node on either the left or right child attribute of the parent.

Node deletion
-------------
Node deletion is one of the more complex operations on a binary search tree, as it often requires some reordering of the tree. 

Deleting leaf nodes is as simple as removing the pointer from the node's parent. The node is then orphaned from the tree and can be garbage collected. Deleting nodes with children becomes more complicated, as left and right children must be reassigned to maintain the integrity of the tree's hierarchy.

Node traversal
--------------

In-order traversal
~~~~~~~~~~~~~~~~~~
During an in-order traversal of the tree, nodes are visited in their ascending, sorted order. This is accomplished by visitng the nodes of the tree in the following order, recursively:
1. The node's left children (Values less than the node itself.)
2. The node itself
3. The node's right children (Values greater than the node itself.)

Pre-order traversal
~~~~~~~~~~~~~~~~~~~
During a pre-order traversal of the tree, nodes are visited such that parents are visited before their children. This can be useful if the node's children need information from their parent and is accomplished by visiting the nodes of the tree in the following order, recursivey:
1. The node itself
2. The node's left children
3. The node's right children

Post-order traversal
~~~~~~~~~~~~~~~~~~~~
During a post-order traversal of the tree, nodes are visited such that the children of a node are visited before the parent. This can be useful if a node's ancestors need information from their children and is accomplished by visiting the nodes of the tree in the following order, recursively:
1. The node's left children
2. The node's right children
3. The node itself 